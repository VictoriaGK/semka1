<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" href="../css/registration_page_style.css">
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
		<script type="text/javascript" src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
	</head>
	<body>
	<nav class= "navbar fixed-top navbar-expand-lg navbar-dark" style="background-color: rgba(11,54,62,0.71);">
        <a class="navbar-brand" href="#">Retro Baby</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link" href="/start">Start <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="/transProd">Produkt</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="/profile">ProfilePage</a>
				</li>
				<li>
					<a class="nav-link" href="/search">Поиск Транспорта</a>
				</li>
			</ul>

		</div>
	</nav>
		<div class="registration_window">
			<form method="POST" enctype="multipart/form-data">
				<div>
					<div class="form_row">
						<div class="form_field">
							<label for="name">Name<br></label>
							<input class="field" type="text" placeholder="${name}" name="name" id="name"  pattern="[A-Z][a-z]*">
						</div>
						<div class="form_field">
							<label for="nickname1">Nickname<br></label>
							<input class="field" type="text" name="nickname1" placeholder="${nickname}"  id="nickname1"  pattern="([0-9]|[A-Z]|[a-z]|_)*" >
						</div>
					</div>
					<div class="form_row">
						<div class="form_field">
							<label for="inputEmail4">Email<br></label>
							<input class="field" id="inputEmail4" type="email" name="email"  placeholder="${email}"  pattern="([0-9]|[a-z]|_|-)+@[a-z]+\.[a-z]{2,3}" >
						</div>
						<div class="form_field">
							<label for="txtNewPassword">Password<br></label>
							<input class="field" type="password" name="txtNewPassword" id="txtNewPassword"  pattern="([0-9]|[A-Z]|[a-z])*">
						</div>
					</div>
					<div class="form_row">
						<div class="form_field">
							<label for="country">Country<br></label>
							<input class="field" type="text" name="country" id="country" placeholder="${country}" >
						</div>
						<div class="form_field">
							<label for="country">Avatar</label>
							<div class="custom-file">
								<label for="customFile">avatar</label>
								<label class="custom-file-label" for="customFile">Choose file</label>
								<input type="file" class="custom-file-input" name="customFile" id="customFile">
							</div>
						</div>
					</div>
					<div>
						<button type="submit" class="btn btn-secondary submit_button">Отрпавить</button>
					</div>
				</div>
			</form>
		</div>
		<#--<script type="text/javascript">-->
			<#--function checkLength(field, minlength, maxlength){-->
				<#--if(field.value.length < minlength){-->
					<#--clearErrorMessage(field);-->
					<#--field.insertAdjacentHTML('afterend', '<div class="error_mes" id="' + field.name + '_error_mes">Must have at least ' + minlength + ' symbols</div>');-->
				<#--}-->
				<#--else if(field.value.length > maxlength){-->
					<#--clearErrorMessage(field);-->
					<#--field.insertAdjacentHTML('afterend', '<div class="error_mes" id="' + field.name + '_error_mes">Must have at most ' + maxlength + ' symbols</div>');-->
				<#--}-->
			<#--}-->
			<#--function clearErrorMessage(field){-->
				<#--if(document.getElementById(field.name + '_error_mes')){-->
					<#--var error_mes = document.getElementById(field.name + '_error_mes');-->
					<#--error_mes.parentNode.removeChild(error_mes);-->
				<#--}-->
			<#--}-->
			<#--function checkNamePatternMatch(field){-->
				<#--var reg = new RegExp(field.pattern);-->
				<#--console.log(reg.exec(field.value) == null);-->
				<#--if(reg.exec(field.value) == null || reg.exec(field.value)[0] != field.value){-->
					<#--clearErrorMessage(field);-->
					<#--field.insertAdjacentHTML('afterend', '<div class="error_mes" id="' + field.name + '_error_mes">Must include only latin letters and begin with uppercase</div>');-->
				<#--}-->
			<#--}-->
			<#--function checkNickPatternMatch(field){-->
				<#--var reg = new RegExp(field.pattern);-->
				<#--if(reg.exec(field.value) == null || reg.exec(field.value)[0] != field.value){-->
					<#--clearErrorMessage(field);-->
					<#--field.insertAdjacentHTML('afterend', '<div class="error_mes" id="' + field.name + '_error_mes">Must include only latin letters, numbers and "_"</div>');-->
				<#--}-->
			<#--}-->
			<#--function checkEmailPatternMatch(field){-->
				<#--var reg = new RegExp(field.pattern);-->
				<#--if(reg.exec(field.value) == null || reg.exec(field.value)[0] != field.value){-->
					<#--clearErrorMessage(field);-->
					<#--field.insertAdjacentHTML('afterend', '<div class="error_mes" id="' + field.name + '_error_mes">Invalid email address</div>');-->
				<#--}-->
			<#--}-->
			<#--function checkPasswordCorrect(field){-->
				<#--var nick = document.getElementById('nickname');-->
				<#--if(nick.value != null && nick.value == field.value){-->
					<#--clearErrorMessage(field);-->
					<#--field.insertAdjacentHTML('afterend', '<div class="error_mes" id="' + field.name + '_error_mes">Password must not be equal to nickname</div>');-->
				<#--}-->
			<#--}-->
		<#--</script>-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	</body>
</html>