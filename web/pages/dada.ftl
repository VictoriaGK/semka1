<html>
<head><title>Acmee Products International</title>
<body>
<h1>Hello !</h1>
<p>These are our latest offers:
<ul>
    <#list transport as trans>
        <div class="card">
            <div class="card1">
                <div class="row no-gutters">
                    <div class="col-md-2">
                        <img src=${imageAva} class="card-img" alt=${trans.name}>
                    </div>


                    <div class="card-body">
                        <div class="row no-gutters">
                            <h5 class="card-title">${trans.name}</h5>
                            <h5 class="card-title">${trans.info}</h5>
                            <table align="center">
                                <tr>
                                    <td></td>
                                </tr>
                            </table>
                            <div class = "button1">
                                <a href="/edit">
                                    <img src="../img/settings.png" class="card-img1"  >
                                </a>
                            </div>

                        </div>

                        <!--   <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                         -->  <p class="card-text"><small class="text-muted">Год Рождения = ${trans.age} </small></p>
                        <p class="card-text"><small class="text-muted">Тип = ${trans.type} </small></p>

                    </div>

                </div>
            </div>
        </div>
    </#list>
</ul>
</body>
</html>