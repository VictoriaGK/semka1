<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/product_page_style.css">
    <link rel="stylesheet" href="../css/styleProfile.css">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
</head>
<body>
<nav class= "navbar fixed-top navbar-expand-lg navbar-dark" style="background-color: rgba(11,54,62,0.71);">
    <a class="navbar-brand" href="#">Retro Baby</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/start">Start <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/transProd">Produkt</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/profile">ProfilePage</a>
            </li>
            <li>
                <a class="nav-link" href="/search">Поиск Транспорта</a>
            </li>
        </ul>

    </div>
</nav>


    <#--<div class="reviews">-->
        <#--<form method="post">-->
            <#--<div>-->
                <#--<input type="text" name="text" id="text" placeholder="Send review">-->
            <#--</div>-->
            <#--<div>-->
                <#--<button type="submit" class="btn btn-secondary">Send review</button>-->
            <#--</div>-->
        <#--</form>-->
        <#--<div class="all_reviews">-->


					<#--<#list messages as mes>-->

                        <#--<div class="review">-->
                            <#--<a href="/profile?id=${mes.iduser}"><h5>${mes.nickname} </h5></a>-->
                            <#--<text>${mes.text}</text>-->
                        <#--</div>-->


                    <#--</#list>-->

        <#--</div>-->
    <#--</div>-->


    <#--<section class="l-page">-->

        <#--<div class = "efg" style="background-color: #ffffff;" >-->
            <#--<div class="content">-->
                <#--<div class="product">-->
                    <#--<img src="${img}" class="card-img1" alt="${name}" >-->
                    <#--<h5 class="card-title">${name}</h5>-->
                    <#--<small class="card-title">${loc}</small>-->
                <#--</div>-->

            <!--    <div class="card" style="width: 18rem;">
                    <img src="image/hes.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <a href="#" class="card-link">Card link</a>
                        <a href="#" class="card-link">Another link</a>
                    </div>

                </div>
    -->




        <#--</div>-->




    <#--</section>-->

<div class="content">
    <div class="product">
        <img src="${img}" class="card-img1" alt="${name}" >
        <h5 class="card-title">${name}</h5>
        <small class="card-title">${loc}</small>
    </div>

    <div class="reviews">
        <div class="all_reviews">


					<#if transports?has_content>
                        <div class = "nn6">
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <div class = "nn0">
                                        <h2>
                                            CПИСОК ТРАНСПОРТА ПО ЛОКАЦИИ </h2>
                                    </div>

                    <#list transports as trans>
                    <div class = "kniga">
                        <div class="card-group" style="background-color: rgba(197,197,197,0.16);">

                            <div class= "card " style="background-color: rgba(197,197,197,0.0);">
                                <img src="${trans.imagePath}" class="card-img-top" alt=${trans.name}>

                            </div>

                            <div class="form-group col-md-11" style="background-color: rgba(197,197,197,0.0);">
                                <div class="card" style="background-color: rgba(197,197,197,0.0);">
                                    <a href="/trans?idtrans=${trans.id}"><h5>${trans.name} </h5></a>
                                    <h4 class="text" id = "text"> ${trans.info}</h4>
                                </div>
                            </div>
                        </div>

                    </div>
                    </#list>

                                </div>
                            </div>
                        </div>
                    </#if>

        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>