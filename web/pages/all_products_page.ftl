<#include 'baseProd.ftl'/>
<#macro content>
		<div class=" products">
			<#list transports as trans>
			<div class="card product">
				<div class="card-body">
                    <a href="/trans?idtrans=${trans.id}">
                        <img src=${trans.imagePath} class="card-img-top" alt=${trans.name} width="400" height="150">
						<div class="card-footer">
                            <p class="card-text">${trans.name}</p>
							</a>
                            <small class="text-muted">
							Год выпуска = ${trans.age}
							</small>
						</div>
                        <#--<p class="card-text"><small class="text-muted">Год выпуска = ${trans.age} </small></p>-->
				</div>
			</div>
			</#list>

		</div>
</#macro>
<@main/>