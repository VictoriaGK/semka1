<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" href="../css/profile_page_style.css">
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
		<script type="text/javascript" src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
	</head>
	<body>
    <nav class= "navbar fixed-top navbar-expand-lg navbar-dark" style="background-color: rgba(11,54,62,0.71);">
        <a class="navbar-brand" href="#">Retro Baby</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/start">Start <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/transProd">Produkt</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/profile">ProfilePage</a>
                </li>
                <li>
                    <a class="nav-link" href="/search">Поиск Транспорта</a>
                </li>
            </ul>

        </div>
    </nav>
    <div class="content">
		<div class="information card">
			<span class="avatar">
				<img src=${imageAva} alt=${nickname}>
			</span>
			<span class="info">
				<div>${fullname}</div>
				<div>Год Рождения ${age}</div>
			</span>
			<span class="settings_button">
				<table align="center">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                        <div class = "button1">
                            <a href="/edit">
                                <img src="../img/settings.png" class="card-img1"  >
                            </a>
                        </div>
                        <div class = "button1">
                            <a href="/logout" class="btn btn-secondary">logout</a>
                        </div>

			</span>
		</div>


        <div class="reviews">
            <div class="all_reviews">

					<#list messages as mes>

                        <div class="review">
                            <a href="/trans?idtrans=${mes.idtrans}"><h5>${mes.nameTrans} </h5></a>
                            <text>${mes.text}</text>
                        </div>


					</#list>

            </div>
        </div>


	</div>






		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	</body>
</html>



<#--<div class="reviews">-->
    <#--<form method="post">-->
        <#--<div>-->
            <#--<input type="text" placeholder="Send review">-->
        <#--</div>-->
        <#--<div>-->
            <#--<button type="submit" class="btn btn-secondary">Send review</button>-->
        <#--</div>-->
    <#--</form>-->
    <#--<div class="all_reviews">-->


					<#--<#list messages as mes>-->

                        <#--<div class="review">-->
                            <#--<a href="/trans?idtrans=${mes.idtrans}"><h5>${mes.nameTrans} </h5></a>-->
                            <#--<text>${mes.text}</text>-->
                        <#--</div>-->


					<#--</#list>-->

    <#--</div>-->
<#--</div>-->

<#--<div class = "nn6">-->
    <#--<div class="form-row">-->
        <#--<div class="form-group col-md-12">-->
            <#--<h2>CПИСОК КНИГ</h2>-->
        <#--<#list messages as mes>-->
            <#--<div class = "kniga">-->
                <#--<div class="card-group" style="background-color: rgba(197,197,197,0.16);">-->

                    <#--<div class= "card " style="background-color: rgba(197,197,197,0.0);">-->
                        <#--<img src="${mes.imagePath}" class="card-img-top" alt=${mes.nameTrans}>-->

                    <#--</div>-->

                    <#--<div class="form-group col-md-11" style="background-color: rgba(197,197,197,0.0);">-->
                        <#--<div class="card" style="background-color: rgba(197,197,197,0.0);">-->
                            <#--<a href="/trans?id=${mes.idtrans}"><h5>${mes.nickname} </h5></a>-->
                            <#--<text>${mes.text}</text>-->
                        <#--</div>-->
                    <#--</div>-->
                <#--</div>-->

            <#--</div>-->

		<#--</#list>-->
        <#--</div>-->
    <#--</div>-->
<#--</div>-->

