<#if name??>

    <p>${name}!</p>
    <#list friends as g>
        <p>${g}, ${name}!</p>
    </#list>
<#else>
    Hello, anonim!
</#if>