<#include 'base.ftl'/>



<#macro content>

<form method="POST">
    <div class = "abc" style="background-color: #ffffff;" >

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputEmail4">Email</label>
                <input type="email" class="form-control" id="inputEmail4" placeholder="${email}"
                       pattern="[0-9a-z_-]+@[0-9a-z_-]+\.[a-z]{2,5}" >
            </div>
            <div class="form-group col-md-6">
                <label for="txtNewPassword">Password</label>
                <input  name="txtNewPassword" type="password" class="form-control" id="txtNewPassword" placeholder="Пароль" pattern="^\S{8,}" onchange="this.setCustomValidity(this.validity.patternMismatch || checkPasswordMatch(this) ? 'Must have at least 8 characters and password must be dont equals nickname' : ''); " >
            </div>


        </div>



        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="name">Name</label>
                <input type="name" class="form-control" id="name" placeholder="${name}"  pattern="^([a-zа-яё]+)$">
            </div>
            <div class="form-group col-md-6">
                <label for="nickname1">Nickname</label>
                <input name = "nickname1" type="nickname" class="form-control" id="nickname1" placeholder="${nickname}" pattern="[0-9a-z_-]+"  >
            </div>
        </div>

        <div class="form-row">


            <div class="form-group col-md-8">
            <label for="country">Avatar</label>
            <div class="custom-file">
                <label for="customFile">avatar</label>
                <label class="custom-file-label" for="customFile">Choose file</label>
                <input type="file" class="custom-file-input" id="customFile">
            </div>
            </div>
            <div class="form-group col-md-4">
                <label for="country">Country</label>
                <div class="dropdown">
                    <a class="btn btn-secondary"  style="width: calc(100%);"href="#" role="button" id="country" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        ${country}
                    </a>

                    <div class="dropdown-menu" aria-labelledby="country">
                        <a class="dropdown-item" href="#">Russia</a>
                        <a class="dropdown-item" href="#">Belarus</a>
                        <a class="dropdown-item" href="#">China</a>
                        <a class="dropdown-item" href="#">Germany</a>
                        <a class="dropdown-item" href="#">Italy</a>
                        <a class="dropdown-item" href="#">Japan</a>
                        <a class="dropdown-item" href="#">Ukraine</a>
                        <a class="dropdown-item" href="#">United Kingdom</a>
                        <a class="dropdown-item" href="#">USA</a>
                    </div>
                </div>
                <script>
                    $(".dropdown-menu  a").click(function(){
                        $(this).parents(".dropdown").find('a:first').html($(this).text() + ' <span class="caret"></span>');
                    });
                </script>
            </div>

        </div>
<div>



</div>

        <button type="submit" class="btn btn-secondary">Submit</button>

    </div>
</form>

</#macro>
<@main/>