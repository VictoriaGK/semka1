<#macro content></#macro>

<#macro display_page>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/style1.css">
    <script type="application/javascript" src="/js/jquery-3.4.1.min.js"></script>
</head>

<body>

<nav class= "navbar fixed-top navbar-expand-lg navbar-dark" style="background-color: rgba(11,54,62,0.71);">
    <a class="navbar-brand" href="#">Library</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/start">Start <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/transProd">Produkt</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="/profile">ProfilePage</a>
            </li>
            <li>
            <a class="nav-link" href="/search">Поиск Транспорта</a>
            </li>
        </ul>

    </div>
</nav>



    <form name = "form1"  method="post">
        <div class = "abc" style="background-color: #ffffff;" >
            <div class="form-group">

                <@content />
            </div>

        </div>



    </form>







</body>
</html>
</#macro>