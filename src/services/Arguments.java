package services;

public class Arguments {
    private static final String dbPath = "jdbc:postgresql://localhost:5432/sem1";
    private static final String dbUser = "postgres";
    private static final String dbPassword = "postgres";
    private static final String dbDriver = "org.postgresql.Driver";

    public static String getDbDriver() {
        return dbDriver;
    }

    public static String getDbPath() {
        return dbPath;
    }

    public static String getDbUser() {
        return dbUser;
    }

    public static String getDbPassword() {
        return dbPassword;
    }
}
