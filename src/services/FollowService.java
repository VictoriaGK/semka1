package services;

import dao.FollowDao;
import models.User;

import java.util.List;

public class FollowService extends model<User> {
FollowDao followDao;

    public FollowService() {
        this.followDao = new FollowDao(getConnection());
    }

    public List<User> findFriends(Integer id){
        return followDao.findFriends(id).orElse(null);
    };
    public List<User> findFollows(Integer userId){
        return followDao.findFollows(userId).orElse(null);

    };
    public List<User> findFollowers(Integer userId){
        return followDao.findFollowers(userId).orElse(null);
    };


}
