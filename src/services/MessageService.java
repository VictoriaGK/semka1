package services;

import dao.MessageDao;
import models.Message;
import models.Transport;
import models.User;

import java.util.List;
import java.util.Optional;

public class MessageService extends model<Message> {
    MessageDao messageDao;

    public
    MessageService() {
        this.messageDao = new MessageDao(getConnection());
    }
    public void saveInBd(Message message) {
        messageDao.save(message);
    }


    public User find(long iduser){
        return messageDao.find(iduser);
    }
    public Optional<List<Message>> findAllByTrans(Transport trans){ return this.messageDao.findAllByTrans(trans);}
    public Optional<List<Message>> findAllByUsers(User users){ return this.messageDao.findAllByUsers(users);}



}
