package services;

import dao.RentDao;
import dao.TransportDao;
import models.Rent;
import models.Transport;
import models.User;

import java.util.List;
import java.util.Optional;

public class RentService extends model<Rent> {
    RentDao rentDao;
    public RentService() {
        this.rentDao = new RentDao(getConnection());
    }

    public Rent find(Long val) {
        return rentDao.find(val);
    }


    public Optional<List<Rent>> findAllBy(String by,Long val ) {
        return rentDao.findAllBy(by, val);
    }
    public Optional<List<Rent>> findAllBy2(String by,Long val ) {
        return rentDao.findAllBy2(by, val);
    }
    public Optional<List<Rent>> findAllByUsers(User user ) {
        return rentDao.findAllByUsers( user);
    }

    public void saveInBd(Rent rent) {
        rentDao.save(rent);
    }

}
