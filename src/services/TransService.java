package services;

import dao.TransportDao;
import models.Magazin;
import models.Transport;
import models.User;

import java.util.List;
import java.util.Optional;

public class TransService extends model<Transport> {
    TransportDao transportDao;

    public TransService() {
        this.transportDao = new TransportDao(getConnection());
    }

    public Transport find(long idtrans) {
        return transportDao.find(idtrans);
    }

    public Optional<List<Transport>> findAll() {
        return transportDao.findAll();
    }
    public List<Transport> getByLikePattern (String pat){
        return this.transportDao.getByLikePattern(pat);
    }
    public Optional<List<Transport>> findAll(Magazin magazin) {
        return (Optional<List<Transport>>) transportDao.findAllTransByMag(magazin);
    }
    public Optional<List<Transport>> findAllTransByMag(Magazin magazin) {
        return transportDao.findAllTransByMag(magazin);
    }

}
