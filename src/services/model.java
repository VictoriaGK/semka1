package services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class model<T> {

    
    public static Connection getConnection() {
        try {
            Class.forName("org.postgresql.Driver");
            return DriverManager.getConnection(Arguments.getDbPath(), Arguments.getDbUser(), Arguments.getDbPassword());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
