package services;

import dao.UserDao;
import models.User;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class UserService extends model<User>{
    UserDao userDao;

    public UserService() {
        this.userDao = new UserDao(getConnection());
    }

    public User getUserByNick(String nickname){
        return userDao.findUserByNick(nickname).orElse(null);
    }

    public void saveInBd(User user) {
        userDao.save(user);
    }

    public User getUserByEmail(String email) {
        return userDao.findUserByEmail(email).orElse(null);
    }
    public User find(long iduser){
        return userDao.find(iduser);
    }
public User validate(String email, String pass) throws SQLException, ClassNotFoundException {
        return userDao.validateUser(email, pass);
}
    public Optional<List<User>> findAll(){ return this.userDao.findAll();}

    public List<User> getByLikePattern (String pat){
        return this.userDao.getByLikePattern(pat);
    }

    public void updateL(long iduser, String param, Long val){
        userDao.updateL(iduser, param, val);
    }
    public void updateS(long iduser, String param, String val){
        userDao.updateS(iduser, param, val);
    }
}