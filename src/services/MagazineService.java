package services;

import dao.MagazinDao;
import dao.MessageDao;
import models.Magazin;
import models.Message;
import models.Transport;
import models.User;

import java.util.List;
import java.util.Optional;

public class MagazineService extends model<Magazin> {
    MagazinDao magazinDao;

    public MagazineService() {
        this.magazinDao = new MagazinDao(getConnection());
    }

    public Magazin find(long idmag) {
        return magazinDao.find(idmag);
    }

    public Optional<List<Magazin>> findAllMagByTrans(Transport transport){
        return magazinDao.findAllMagByTrans(transport);
    }

}
