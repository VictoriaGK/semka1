package servlets;

import models.Message;
import models.Transport;
import models.User;
import services.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;


@WebServlet(urlPatterns = {"/trans"})
public class TransServ extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        HttpSession session = req.getSession();
        User user1 = (User) session.getAttribute("user_current");

        Transport transport = (Transport) session.getAttribute("transport");

        TransService transServ = new TransService();
        MessageService messageService = new MessageService();
        MagazineService magazineService = new MagazineService();
        String otherTransId = req.getParameter("idtrans");


            Transport transport1 = transServ.find(Integer.valueOf(otherTransId));

            req.setAttribute("transport", transport1);
            req.setAttribute("name", transport1.getName());
            req.setAttribute("info", transport1.getInfo());
            req.setAttribute("age", transport1.getAge());
            req.setAttribute("type", transport1.getType());
            req.setAttribute("id", transport1.getId());


            req.setAttribute("imageAva", transport1.getImagePath());
            // req.setAttribute("friends", users1);

            req.setAttribute("messages", (messageService.findAllByTrans(transport1).get()));
        System.out.println(magazineService.findAllMagByTrans(transport1).get());
            req.setAttribute("magazin", (magazineService.findAllMagByTrans(transport1).get()));

            req.setAttribute("name", transport1.getName());




            RequestDispatcher dispatcher = req.getRequestDispatcher("pages/product_page.ftl");
            dispatcher.forward(req,resp);





    }





    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        HttpSession session = req.getSession();

        User user1 = (User) session.getAttribute("user_current");
        resp.setCharacterEncoding("UTF-8");
        String otherTransId = req.getParameter("idtrans");



        if (user1 == null) {
            resp.sendRedirect("/login");
        } else {

            req.setCharacterEncoding("UTF-8");

            String text = req.getParameter("text");
            String  otherId = String.valueOf(user1.getId());


            Message message = new Message(text, Long.parseLong(otherTransId),Long.parseLong(otherId));
            MessageService messageService = new MessageService();
            messageService.saveInBd(message);
            session.setAttribute("user_current", user1);

            resp.sendRedirect("/trans?idtrans=" + otherTransId);
        }

        // resp.sendRedirect(req.getContextPath() + "/start");


    }

}

