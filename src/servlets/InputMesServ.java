package servlets;


import models.Message;
import models.Transport;
import models.User;
import services.MessageService;
import services.TransService;
import services.UserService;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet("/input")
public class InputMesServ extends HttpServlet {





    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        HttpSession session = req.getSession();

        User user1 = (User) session.getAttribute("user_current");
        resp.setCharacterEncoding("UTF-8");
        String otherTransId = req.getParameter("idtrans");



        if (user1 == null) {
            resp.sendRedirect("/login");
        } else {

                 req.setCharacterEncoding("UTF-8");

                String text = req.getParameter("text");
                String  otherId = String.valueOf(user1.getId());


                Message message = new Message(text, Long.parseLong(otherTransId),Long.parseLong(otherId));
                MessageService messageService = new MessageService();
                messageService.saveInBd(message);

                }
                session.setAttribute("user_current", user1);

                resp.sendRedirect("/trans?idtrans=" + otherTransId);
                // resp.sendRedirect(req.getContextPath() + "/start");


    }

}

