package servlets;


import models.User;
import services.FollowService;
import services.MessageService;
import services.RentService;
import services.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;


@WebServlet(urlPatterns = {"/profile"})
public class ProfileServ extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        HttpSession session = req.getSession();
        User potentialUser = (User) session.getAttribute("user_current");

        UserService userService = new UserService();
        MessageService messageService = new MessageService();
        RentService rentService = new RentService();


        String otherUserId = req.getParameter("id");


            if ((otherUserId == null || Integer.valueOf(otherUserId) == session.getAttribute("user_current"))&& potentialUser!=null) {



                req.setAttribute("nickname", potentialUser.getNickname());
                req.setAttribute("fullname", potentialUser.getName());
                req.setAttribute("age", potentialUser.getAge());
                req.setAttribute("imageAva", potentialUser.getAvatar());
               // req.setAttribute("friends", users1);
                resp.setContentType("text/html");
                req.setAttribute("messages", (messageService.findAllByUsers(potentialUser).get()));
                req.setAttribute("rents", rentService.findAllByUsers(potentialUser).get());


                RequestDispatcher dispatcher = req.getRequestDispatcher("pages/profileMy.ftl");
                dispatcher.forward(req,resp);

            } else {
                if (otherUserId != null) {
                    Optional<User> potentialOtherUser = Optional.ofNullable(userService.find(Integer.valueOf(otherUserId)));
                    User otherUser = null;
                    if (potentialOtherUser.isPresent()) {
                        otherUser = potentialOtherUser.get();
                        req.setAttribute("nickname", potentialOtherUser.get().getNickname());
                        req.setAttribute("fullname", potentialOtherUser.get().getName());
                        req.setAttribute("age", potentialOtherUser.get().getAge());
                        req.setAttribute("imageAva", potentialOtherUser.get().getAvatar());
                        req.setAttribute("messages", (messageService.findAllByUsers(potentialOtherUser.get())));

                    }
                    req.setAttribute("messages", (messageService.findAllByUsers(potentialUser).get()));
                    req.getRequestDispatcher("pages/otherProfile.ftl").forward(req, resp);
                } else{
                    resp.sendRedirect("/login");
                }
            }
    }

}










