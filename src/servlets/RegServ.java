package servlets;


import models.User;
import services.HashHelper;
import services.UserService;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@MultipartConfig

@WebServlet("/reg")
public class RegServ extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("user_current");
        resp.setCharacterEncoding("UTF-8");

        Cookie[] cookies = req.getCookies();
        if (user == null && cookies != null) {
            for (Cookie cookie: cookies) {
                try {
                    user = new UserService().validate(cookie.getName(), cookie.getValue());
                    if (user != null) {
                        break;
                    }
                } catch (SQLException | ClassNotFoundException e) {
                    System.out.println("500");
                }
            }
        }
        if (user != null) {
            session.setAttribute("user_current", user);
            resp.sendRedirect("/profile");
        } else {
            resp.setContentType("text/html");
            RequestDispatcher dispatcher = req.getRequestDispatcher("pages/registration_page.html");
            dispatcher.forward(req, resp);
        }
    }







    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");


        UserService userService = new UserService();
        User user1 = userService.getUserByNick(req.getParameter("nickname1"));
        User user2 = userService.getUserByEmail(req.getParameter("email"));
        Part part = req.getPart("customFile");
        String remember = req.getParameter("remember");
        String partName = Paths.get(part.getSubmittedFileName()).getFileName().toString();



        String[] filenames = partName.split("\\.");
                InputStream fileContent = part.getInputStream();
                File uploads = new File("/Users/vikki/IdeaProjects/Sem12/semKa/out/artifacts/semKa_war_exploded/img");

                File file = File.createTempFile("img", "." + filenames[1], uploads);

                Files.copy(fileContent, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
        String path = "../img/"+file.getPath().split("/")[10];



        Pattern p = Pattern.compile("^([0-9]|[a-z]|_|-)+@[a-z]+\\.[a-z]{2,3}");
        Matcher m = p.matcher(req.getParameter("email"));
       if(!m.matches()){
           resp.sendRedirect("/reg");
       }
        Pattern p1 = Pattern.compile(req.getParameter("txtNewPassword"));
        Matcher m1 = p1.matcher(req.getParameter("txtConfirmPassword"));
        if(!m1.matches()){
            resp.sendRedirect("/reg");
        }

        Pattern p2 = Pattern.compile("([0-9]|[A-Z]|[a-z]|_)*");
        Matcher m2 = p2.matcher(req.getParameter("nickname1"));
        if(!m2.matches()){
            resp.sendRedirect("/reg");
        }


        if (user1 == null && user2 == null) {
            req.setCharacterEncoding("UTF-8");
            User user = new User(

                    req.getParameter("nickname1"),
                    req.getParameter("email"),
                    path,
                    req.getParameter("name"),
                    HashHelper.getHash(req.getParameter("txtNewPassword")),
                    Long.parseLong(req.getParameter("ageN")),
                    req.getParameter("country"));


            userService.saveInBd(user);

            HttpSession session = req.getSession();



            session.setAttribute("user_current", user);
            resp.sendRedirect("/profile");
        } else {
            doGet(req, resp);
        }
    }



}

