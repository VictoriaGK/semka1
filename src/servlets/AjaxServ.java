package servlets;



import dao.UserDao;

import models.Transport;
import models.User;

import org.json.JSONArray;
import org.json.JSONObject;
import services.TransService;
import services.UserService;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@WebServlet("/dosearch")
public class AjaxServ extends HttpServlet {
    private TransService transService;

    @Override
    public void init() {
        transService = new TransService();
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        String query = req.getParameter("query");

        List<Transport> transports = transService.getByLikePattern(query);

        JSONArray ja = new JSONArray();
        for (Transport transport: transports) {
            ja.put(new JSONObject(transport));
        }
        JSONObject jo = new JSONObject();
        jo.put("objects", ja);


        resp.setContentType("text/json");
     //   req.setAttribute("list", (jo.toString()));
        resp.getWriter().write(jo.toString());

    }
}
