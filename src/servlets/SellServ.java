package servlets;


import models.Message;
import models.Rent;
import models.Transport;
import models.User;
import services.MessageService;
import services.RentService;
import services.TransService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/sell")
public class SellServ extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        HttpSession session = req.getSession();

        User user = (User) session.getAttribute("user_current");
        resp.setCharacterEncoding("UTF-8");

        if (user == null) {
            resp.sendRedirect("/login");
        } else {

            req.setAttribute("name", user.getName());
            resp.setContentType("text/html");

            RequestDispatcher dispatcher = req.getRequestDispatcher("pages/sell.ftl");
            dispatcher.forward(req,resp);

        }
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setCharacterEncoding("UTF-8");
        HttpSession session = req.getSession();
        RentService rentService = new RentService();
        User user1 = (User) session.getAttribute("user_current");
        TransService transService = new TransService();
        String otherTransId = req.getParameter("idtrans");
        resp.setCharacterEncoding("UTF-8");



        Transport transport = transService.find(Long.parseLong(otherTransId));





        if (user1 == null) {
            resp.sendRedirect("/login");
        } else {
            Rent rent = new Rent(Long.parseLong(req.getParameter("tel")),transport.getId().longValue() ,user1.getId().longValue());


            rentService.saveInBd(rent);
            req.setCharacterEncoding("UTF-8");
            String text = req.getParameter("tel");


        }
        session.setAttribute("user_current", user1);
        resp.sendRedirect("/profile");
        // resp.sendRedirect(req.getContextPath() + "/start");


    }

}


