package servlets;

import models.User;
import services.FollowService;
import services.HashHelper;
import services.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@WebServlet("/edit")
@MultipartConfig
public class EditServ  extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        HttpSession session = req.getSession();
        resp.setCharacterEncoding("UTF-8");
        User user = (User) session.getAttribute("user_current");
        UserService userService = new UserService();


        Pattern p = Pattern.compile("^([0-9]|[a-z]|_|-)+@[a-z]+\\.[a-z]{2,6}");
        Matcher m = p.matcher(req.getParameter("email"));
        if(!m.matches()){
            resp.sendRedirect("/reg");
        }

        Pattern p2 = Pattern.compile("([0-9]|[A-Z]|[a-z]|_)*");
        Matcher m2 = p2.matcher(req.getParameter("nickname1"));
        if(!m2.matches()){
            resp.sendRedirect("/reg");
        }


        if(!(req.getParameter("email").equals(""))){
                userService.updateS(user.getId(),"email", req.getParameter("email") );
            }


            if(!(req.getParameter("nickname1").equals(""))){
                userService.updateS(user.getId(),"nickname", req.getParameter("nickname1") );
            }

        if(!(req.getParameter("name").equals(""))){
            userService.updateS(user.getId(),"namefull", req.getParameter("name") );
        }

            if(!(req.getParameter("txtNewPassword").equals(""))){
                userService.updateS(user.getId(),"password", HashHelper.getHash(req.getParameter("txtNewPassword")) );
            }

            if(!(req.getParameter("country").equals(""))){
                userService.updateS(user.getId(),"city", (req.getParameter("country")) );
            }

            if((req.getParameter("customFile"))!=null){
                Part part = req.getPart("customFile");
                String partName = Paths.get(part.getSubmittedFileName()).getFileName().toString();

                String[] filenames = partName.split("\\.");
                InputStream fileContent = part.getInputStream();
                File uploads = new File("/Users/vikki/IdeaProjects/Sem12/semKa/out/artifacts/semKa_war_exploded/img");

                File file = File.createTempFile("img", "." + filenames[1], uploads);

                Files.copy(fileContent, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
                String path = "../img/"+file.getPath().split("/")[10];

                userService.updateS(user.getId(),"avatarpath", path );
            }

            User user1 = userService.find(user.getId());

            session.setAttribute("user_current", user1);
            resp.setCharacterEncoding("UTF-8");
            resp.setContentType("text/html");

            resp.sendRedirect( "/edit");



    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        User potentialUser = (User) session.getAttribute("user_current");
        resp.setCharacterEncoding("UTF-8");

        String otherUserId = req.getParameter("id");

        if (otherUserId == null || Integer.valueOf(otherUserId) == session.getAttribute("user_current")) {


            req.setAttribute("name", potentialUser.getName());
            req.setAttribute("nickname", potentialUser.getNickname());
            req.setAttribute("email", potentialUser.getEmail());
            req.setAttribute("age", potentialUser.getAge());
            req.setAttribute("country", potentialUser.getCity());


            resp.setContentType("text/html");

            RequestDispatcher dispatcher = req.getRequestDispatcher("pages/edit_page.ftl");
            dispatcher.forward(req, resp);


        }
    }
}
