package servlets;

import models.User;
import services.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/logout")
public class LogOutServ extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        HttpSession session = req.getSession();
        Cookie cMail = new Cookie("cookmail", null);
        Cookie cPassword = new Cookie("cookpass", null);
        Cookie cRemember = new Cookie("cookrem", null);
        cMail.setMaxAge(0);
        cPassword.setMaxAge(0);
        cRemember.setMaxAge(0);
        resp.addCookie(cMail);
        resp.addCookie(cPassword);
        resp.addCookie(cRemember);
        session.removeAttribute("user_current");
        resp.sendRedirect("/login");

    }
}

