package servlets;


import models.Magazin;
import models.Message;
import models.Transport;
import models.User;
import services.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;


@WebServlet(urlPatterns = {"/magazin"})
public class MagazinServ extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        HttpSession session = req.getSession();
        User user1 = (User) session.getAttribute("user_current");

        Magazin magazin = (Magazin) session.getAttribute("magazin");

        TransService transServ = new TransService();
        MagazineService magazineService = new MagazineService();
        MessageService messageService = new MessageService();
        String otherMagId = req.getParameter("idmag");


        Magazin magazin1 = magazineService.find(Integer.valueOf(otherMagId));


        req.setAttribute("name", magazin1.getNamemag());
        req.setAttribute("img", magazin1.getImgmag());
        req.setAttribute("loc", magazin1.getLoc());



        // req.setAttribute("friends", users1);
        System.out.println(transServ.findAllTransByMag(magazin1).get());

        req.setAttribute("transports", (transServ.findAllTransByMag(magazin1)).get());




        RequestDispatcher dispatcher = req.getRequestDispatcher("pages/magaz.ftl");
        dispatcher.forward(req,resp);





    }



}

