package servlets;

import models.User;
import services.HashHelper;
import services.UserService;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/login")
public class LogServ extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        HttpSession session = req.getSession();

        User user = (User) session.getAttribute("user_current");



        if (user != null) {
            session.setAttribute("user_current", user);
            resp.sendRedirect("/profile");
        } else {

            resp.setContentType("text/html");
            RequestDispatcher dispatcher = req.getRequestDispatcher("pages/login_page.html");
            dispatcher.forward(req,resp);

        }
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession();

        User user1 = (User) session.getAttribute("user_current");



        if (user1 != null) {
            resp.sendRedirect("/profile");
        } else {

        String remember = req.getParameter("remember");

        UserService userService = new UserService();
        User user = userService.getUserByEmail(req.getParameter("email"));
        if (user != null && user.getPassword().equals(HashHelper.getHash(req.getParameter("password")))) {



            if (remember != null) {
                Cookie cMail = new Cookie("cookmail", req.getParameter("email"));
                Cookie cPassword = new Cookie("cookpass", HashHelper.getHash(req.getParameter("password")));
                cMail.setMaxAge(60 * 60 * 24 * 10000);
                cPassword.setMaxAge(60 * 60 * 24 * 10000);
                resp.addCookie(cMail);
                resp.addCookie(cPassword);
            }

            session.setAttribute("user_current", user);
            resp.sendRedirect("/profile");
            // resp.sendRedirect(req.getContextPath() + "/start");

        } else {
            doGet(req, resp);
        }
    }


    }

}
