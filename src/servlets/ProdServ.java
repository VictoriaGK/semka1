package servlets;

import models.Transport;
import services.TransService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@WebServlet(urlPatterns = {"/transProd"})
public class ProdServ extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        HttpSession session = req.getSession();


        TransService transServ = new TransService();





        req.setAttribute("transports", (transServ.findAll().get()));


//        for (Transport transport1: transports) {
//
//            req.setAttribute("name", transport1.getName());
//            req.setAttribute("info", transport1.getInfo());
//            req.setAttribute("age", transport1.getAge());
//            req.setAttribute("type", transport1.getType());
//            req.setAttribute("imageAva", transport1.getImage());
//        }




        // req.setAttribute("friends", users1);

        resp.setContentType("text/html");

        RequestDispatcher dispatcher = req.getRequestDispatcher("pages/all_products_page.ftl");
        dispatcher.forward(req,resp);


    }

}
