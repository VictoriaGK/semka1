package servlets;

import dao.UserDao;
import models.User;
import services.UserService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


@WebFilter(filterName = "FilterServ", urlPatterns = {"/*"})
public class FilterServ implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;


        String cMail = null;
        String cookPassword = null;
        for (Cookie cookie : request.getCookies()) {
            if (cookie.getName().equals("cookmail")) {
                cMail = cookie.getValue();
            }
            if (cookie.getName().equals("cookpass")) {
                cookPassword = cookie.getValue();
            }

        }

        UserService userService = new UserService();
        User user1 = userService.getUserByEmail(cMail);
        if(cMail!= null && cookPassword!=null && user1.getPassword().equals(cookPassword)) {
            request.getSession().setAttribute("user_current", user1);
        }
        chain.doFilter(req, resp);


    }

    public void init(FilterConfig config) throws ServletException {

    }

}
