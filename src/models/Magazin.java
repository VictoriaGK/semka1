package models;

import java.util.List;

public class Magazin {
    private Integer idmag;
    private String namemag;
    private String imgmag;
    private String loc;
    private List<Transport> transports;

    public Magazin(Integer idmag, String namemag, String imgmag, String loc) {
        this.idmag = idmag;
        this.namemag = namemag;
        this.imgmag = imgmag;
        this.loc = loc;

    }

    public Magazin(String namemag, String imgmag, String loc) {
        this.namemag = namemag;
        this.imgmag = imgmag;
        this.loc = loc;

    }

    public Integer getIdmag() {
        return idmag;
    }

    public void setIdmag(Integer idmag) {
        this.idmag = idmag;
    }

    public String getNamemag() {
        return namemag;
    }

    public void setNamemag(String namemag) {
        this.namemag = namemag;
    }

    public String getImgmag() {
        return imgmag;
    }

    public void setImgmag(String imgmag) {
        this.imgmag = imgmag;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public List<Transport> getTransports() {
        return transports;
    }

    public void setTransports(List<Transport> transports) {
        this.transports = transports;
    }
}
