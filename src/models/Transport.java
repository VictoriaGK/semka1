package models;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data

@NoArgsConstructor
public class Transport {
    private Integer id;
    private String name;
    private String info;

    private Integer age;
    private String type;
    private String imagePath;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getInfo() {
        return info;
    }

    public Integer getAge() {
        return age;
    }

    public String getType() {
        return type;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Transport(Integer id, String name, String info, Integer age, String type, String imagePath) {
        this.id = id;
        this.name = name;
        this.info = info;
        this.age = age;
        this.type = type;
        this.imagePath = imagePath;
    }

    public Transport(String name, String info, Integer age, String type, String imagePath) {
        this.name = name;
        this.info = info;
        this.age = age;
        this.type = type;
        this.imagePath = imagePath;
    }
}
