package models;

public class Message {
    private Integer idcom;
    private String text;
    private Long idtrans;
    private Long iduser;
    private String nickname;
    private String imagePath;
    private String nameTrans;

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getNameTrans() {
        return nameTrans;
    }

    public void setNameTrans(String nameTrans) {
        this.nameTrans = nameTrans;
    }

    public Message(Integer idcom, String text, Long idtrans, Long iduser, String nickname, String imagePath, String nameTrans) {
        this.idcom = idcom;
        this.text = text;
        this.idtrans = idtrans;
        this.iduser = iduser;
        this.nickname = nickname;
        this.imagePath = imagePath;
        this.nameTrans = nameTrans;
    }

    public Message(Integer idcom, String text, Long idtrans, Long iduser) {
        this.idcom = idcom;
        this.text = text;
        this.idtrans = idtrans;
        this.iduser = iduser;

    }

    public Message(String text, Long idtrans, Long iduser) {
        this.text = text;
        this.idtrans = idtrans;
        this.iduser = iduser;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Integer getIdcom() {
        return idcom;
    }

    public void setIdcom(Integer idcom) {
        this.idcom = idcom;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getIdtrans() {
        return idtrans;
    }

    public void setIdtrans(Long idtrans) {
        this.idtrans = idtrans;
    }

    public Long getIduser() {
        return iduser;
    }

    public void setIduser(Long iduser) {
        this.iduser = iduser;
    }
}
