package models;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.List;



@AllArgsConstructor
@NoArgsConstructor
public class User {
    private Integer id;
    private String nickname;
    private String email;

    private Long age;
    private String avatar;
    private List<Transport> transports;
    private String password;
    private String name;
    private String city;


    public User(Integer id, String nickname, String email,
                 String avatar, String name, String password, Long age, String city) {
        this.id = id;
        this.nickname = nickname;
        this.email = email;

        this.avatar = avatar;
        this.name = name;
        this.password = password;
        this.age = age;
        this.city = city;
    }

    public User(String nickname, String email,
                 String avatar, String name, String password,Long age, String  city) {
        this.nickname = nickname;
        this.email = email;

        this.avatar = avatar;
        this.name = name;
        this.password = password;
        this.age = age;
        this.city = city;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public List<Transport> getTransports() {
        return transports;
    }

    public void setTransports(List<Transport> transports) {
        this.transports = transports;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

}



