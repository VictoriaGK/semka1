package models;

public class Rent {
    private Long idRent;
    private Long telNumb;
    private Long idtrans;
    private Long iduser;
    private String imgtrans;
    private String nametrans;
    private String infotrans;

    public Rent(Long telNumb, Long idtrans, Long iduser) {
        this.telNumb = telNumb;
        this.idtrans = idtrans;
        this.iduser = iduser;
    }

    public Rent(Long idRent, Long telNumb, Long idtrans, Long iduser) {
        this.idRent = idRent;
        this.telNumb = telNumb;
        this.idtrans = idtrans;
        this.iduser = iduser;
    }

    public Rent(Long telNumb, Long idtrans, Long iduser, String imgtrans, String nametrans, String infotrans) {
        this.telNumb = telNumb;
        this.idtrans = idtrans;
        this.iduser = iduser;
        this.imgtrans = imgtrans;
        this.nametrans = nametrans;
        this.infotrans = infotrans;
    }

    public String getInfotrans() {
        return infotrans;
    }

    public void setInfotrans(String infotrans) {
        this.infotrans = infotrans;
    }

    public String getImgtrans() {
        return imgtrans;
    }

    public void setImgtrans(String imgtrans) {
        this.imgtrans = imgtrans;
    }

    public String getNametrans() {
        return nametrans;
    }

    public void setNametrans(String nametrans) {
        this.nametrans = nametrans;
    }

    public Long getIdRent() {
        return idRent;
    }


    public Long getTelNumb() {
        return telNumb;
    }

    public void setTelNumb(Long telNumb) {
        this.telNumb = telNumb;
    }

    public Long getIdtrans() {
        return idtrans;
    }

    public void setIdtrans(Long idtrans) {
        this.idtrans = idtrans;
    }

    public Long getIduser() {
        return iduser;
    }

    public void setIduser(Long iduser) {
        this.iduser = iduser;
    }
}

