package dao;

import models.User;
import services.UserService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class FollowDao implements OneDao<User> {

    Connection connection;

    public FollowDao(Connection connection) {
        this.connection = connection;
    }


    public Optional<List<User>> findFriends(Integer id) {

        List<Long> potentFriendsIds = new ArrayList<>();
        UserService userService = new UserService();
        List<User> friends = new ArrayList<>();

        try (PreparedStatement ps = connection.prepareStatement("SELECT user2 FROM follows WHERE user1"+" = ?")) {
            ps.setLong(1, id);

            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next()) {
                potentFriendsIds.add(resultSet.getLong("user2"));

            }
        } catch (
                SQLException e) {
            throw new IllegalStateException(e);
        }


        try (PreparedStatement ps = connection.prepareStatement("SELECT user1 FROM follows WHERE user1"+" = ?"+ " AND user2"+" = ?")) {
            for (Long potId : potentFriendsIds) {

                ps.setLong(1, potId);
                ps.setLong(2, id);

                ResultSet resultSet = ps.executeQuery();
                if (resultSet.next()) {

                    Optional<User> userOptional = Optional.of((userService.find(resultSet.getInt("user1"))));
                    userOptional.ifPresent(friends::add);
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        if (friends.size() == 0) return Optional.empty();
        else return Optional.of(friends);

    }




    public Optional<List<User>> findFollowers(Integer userId) {
        String sql = "SELECT user1 FROM follows WHERE user2 = ?;";
        List<Integer> potentialFollowers = new ArrayList<>();
        UserDao userDao = new UserDao(connection);
        List<User> followers = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, userId);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) potentialFollowers.add(resultSet.getInt("user1"));
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        if (potentialFollowers.size() != 0) {
            Optional<List<User>> potentialFriends = findFriends(userId);
            List<User> friends;
            if (potentialFriends.isPresent()) {
                friends = potentialFriends.get();
                for (Integer i : potentialFollowers) {
                    for (User u : friends) {
                        if (i != u.getId()) followers.add(userDao.find(i));
                    }
                }
            }
        }
        if (followers.size() == 0) return Optional.empty();
        return Optional.of(followers);

    }


    public Optional<List<User>> findFollows(Integer userId) {
        String sql = "SELECT user2 FROM follows WHERE user1 = ?";
        List<Integer> potentialFollows = new ArrayList<>();
        List<User> follows = new ArrayList<>();
        UserDao userDao = new UserDao(connection);

        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, userId);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) potentialFollows.add(resultSet.getInt("user2"));
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        if (potentialFollows.size() != 0) {
            Optional<List<User>> potentialFriends = findFriends(userId);
            List<User> friends;
            if (potentialFriends.isPresent()) {
                friends = potentialFriends.get();
                for (Integer i : potentialFollows) {
                    for (User u : friends) {
                        if (i != u.getId()) follows.add(userDao.find(i));
                    }
                }
            }
        }
        if (follows.size() == 0) return Optional.empty();
        return Optional.of(follows);
    }

    @Override
    public void create() {

    }

    @Override
    public User find(long id) {
        return null;
    }

    @Override
    public void update() {

    }

    @Override
    public void save(User model) {

    }

    @Override
    public void delete(long id) {

    }
}
