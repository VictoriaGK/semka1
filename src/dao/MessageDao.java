package dao;

import models.Message;
import models.Transport;
import models.User;
import services.TransService;
import services.UserService;

import javax.xml.stream.events.Comment;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MessageDao implements OneDao<User> {

    Connection connection;

    public MessageDao(Connection connection) {
        this.connection = connection;
    }

    private Message createMessage(ResultSet rs) throws SQLException {
        Message message = null;
        UserService userService = new UserService();
        User user  = userService.find(rs.getLong("iduser"));
        try {
            message = new Message(
                    rs.getInt("idcom"),
                    rs.getString("text"),
                    rs.getLong("idrans"),
                    rs.getLong("iduser"));
            message.setNickname(user.getNickname());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return message;
    }

    @Override
    public void create() {

    }

    @Override
    public User find(long id) {
        return null;
    }


    public Optional<List<Message>> findAllByTrans(Transport trans) {

        long idtrans = trans.getId();
        List <Message> messages = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM comments WHERE idrans" +" = ?")) {

            ps.setLong(1,idtrans);

            ResultSet rs = ps.executeQuery();

            while(rs.next()){


                Message message2 = (createMessage(rs));

                messages.add(message2);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.ofNullable(messages);
    }

    private Message createMessage2(ResultSet rs) throws SQLException {
        Message message = null;
        UserService userService = new UserService();
        TransService transService = new TransService();
        Transport trans = transService.find(rs.getLong("idrans"));
        User user  = userService.find(rs.getLong("iduser"));
        try {
            message = new Message(
                    rs.getInt("idcom"),
                    rs.getString("text"),
                    rs.getLong("idrans"),
                    rs.getLong("iduser"));
            message.setNickname(user.getNickname());
            message.setImagePath(trans.getImagePath());
            message.setNameTrans(trans.getName());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return message;
    }



    public Optional<List<Message>> findAllByUsers(User users) {

        long iduser = users.getId();
        List <Message> messages = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM comments WHERE iduser" +" = ?")) {

            ps.setLong(1,iduser);

            ResultSet rs = ps.executeQuery();

            while(rs.next()){


                Message message2 = (createMessage2(rs));

                messages.add(message2);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.ofNullable(messages);
    }


    @Override
    public void update() {

    }

    @Override
    public void save(User model) {

    }

    public void save(Message message) {


        if (message == null) throw new NullPointerException("User can't be null");
        String SQL_INSERT = "INSERT INTO " +
                "comments (text, idrans," +
                " iduser) VALUES (?, ?, ?)";
        try (PreparedStatement ps = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, message.getText());
            ps.setLong(2, message.getIdtrans());
            ps.setLong(3,message.getIduser());
            int affectedRows = ps.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException();
            }
            try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    message.setIdcom(generatedKeys.getInt("idcom"));
                } else {
                    throw new SQLException();
                }
            }
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void delete(long id) {

    }
}
