package dao;


import models.Magazin;
import models.Transport;
import models.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TransportDao implements OneDao<Transport> {
    Connection connection;

    public TransportDao(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void create() {
    }

    @Override
    public void update() {
    }


    @Override
    public void save(Transport model) {
   try (PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO transport (idtrans, nametrans, infotrans, agetrans, typetrans, imgtrans) VALUES (?,?,?,?, ?, ?)",
                Statement.RETURN_GENERATED_KEYS);) {
            statement.setInt(1, model.getId());
            statement.setString(2, model.getName());
            statement.setString(3, model.getInfo());
            statement.setInt(4, model.getAge());
            statement.setString(5, model.getType());
            statement.setString(6, model.getImagePath());
            DaoHelper<Transport> daoHelper = new DaoHelper<>();
            daoHelper.checkingСhanges(statement);
        } catch (SQLException e) {
            //Если сохранений провалилось, обернём пойманное исключение в непроверяемое и пробросим дальше(best-practise)
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void delete(long id) {

    }

    Optional<List<Transport>> findAllTransByUser(User model) {
        List<Transport> transports = null;
        try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM transport WHERE idtrans=?",
                Statement.RETURN_GENERATED_KEYS)) {
            ps.setLong(1, model.getId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                transports.add(createTransByRS(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.ofNullable(transports);
    }

    public Optional<List<Transport>> findAllTransByMag(Magazin model) {
        List<Transport> transports = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM transport WHERE idmag" +" = ?")) {
            ps.setLong(1, model.getIdmag());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Transport transport2 = (createTransByRS(rs));

                transports.add(transport2);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.ofNullable(transports);
    }









    private Transport createTransByRS(ResultSet rs) throws SQLException {
        Transport transport = null;
        try {
            transport =  new Transport(rs.getInt("idtrans"),
                    rs.getString("nametrans"),
                    rs.getString("infotrans"),
                    rs.getInt("agetrans"),
                    rs.getString("typetrans"),
                    rs.getString("imgtrans"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return transport;
    }

    public Transport find(long idtrans) {

        Transport tr = null;
        try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM transport WHERE idtrans" +" = ?")) {

            ps.setLong(1,idtrans);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
               tr  = createTransByRS(rs);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tr;
    }


    public Optional<List<Transport>> findAll() {
        List<Transport> transports = new ArrayList<>();
        String sqlAll = "SELECT * FROM transport";
        try (PreparedStatement ps = connection.prepareStatement(sqlAll)) {
            ResultSet rs = ps.executeQuery();

            while(rs.next()){


                Transport transport2 = (createTransByRS(rs));

                transports.add(transport2);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.ofNullable(transports);
    }



    public List<Transport> getByLikePattern(String pattern) {
        try {

            PreparedStatement ps = connection.prepareStatement(
                    "select * from transport where nametrans like ?"
            );
            ps.setString(1, "%" + pattern + "%");
            ResultSet rs = ps.executeQuery();
            List<Transport> transports = new ArrayList<>();
            while (rs.next()) {
                transports.add(new Transport(rs.getInt("idtrans"),
                        rs.getString("nametrans"),
                        rs.getString("infotrans"),
                        rs.getInt("agetrans"),
                        rs.getString("typetrans"),
                        rs.getString("imgtrans")));

            }
            return transports;
        } catch (SQLException e) {
            e.printStackTrace();

        }
        return null;
    }

}
