package dao;

public interface OneDao<T> {
    void create();
    T find(long id);
    void update();
    void save(T model);
    void delete(long id);
}
