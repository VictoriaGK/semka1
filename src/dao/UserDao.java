package dao;

import models.Transport;
import models.User;
import services.RowMapper;

import javax.servlet.http.Part;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

public class UserDao implements OneDao<User> {
    Connection connection;

    public UserDao(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void create() {
    }

    public UserDao() {
    }

    @Override
    public User find(long iduser) {

        User user = null;
        try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM usertrans1 WHERE iduser" +" = ?")) {

            ps.setLong(1,iduser);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                user = createUser(rs);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }
    public void updateS(long iduser, String param, String val ){

        try (PreparedStatement ps = connection.prepareStatement("UPDATE usertrans1 SET " + param +" = ?" + " WHERE iduser"+ " = ?")) {
            ps.setString(1,val);
            ps.setLong(2,iduser);
            ps.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    public void updateL(long iduser, String param, Long val ){

        try (PreparedStatement ps = connection.prepareStatement("UPDATE usertrans1 SET " + param + " = ?" + " WHERE iduser"+ " = ?")) {

            ps.setLong(1,val);
            ps.setLong(2,iduser);
            ps.execute();


        } catch (SQLException e) {
            e.printStackTrace();
        }


    }



    public User find(String by,String eql){
        User user = null;
        try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM usertrans1 WHERE "+ by +" = ?")) {
            ps.setString(1, eql);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                user = createUser(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }


    public Optional<User> findUserByNick(String nickname) {
        return Optional.ofNullable(find("nickname",nickname));
    }

    public Optional<User> findUserById(String iduser) {
        return Optional.ofNullable(find("iduser",iduser));
    }


    public Optional<User> findUserByEmail(String email) {
        return Optional.ofNullable(find("email",email));

    }

    private User createUser(ResultSet rs) throws SQLException {
        User user = null;
        try {
            user = new User(
                    rs.getInt("idUser"),
                    rs.getString("nickname"),
                    rs.getString("email"),

                    rs.getString("avatarPath"),
                    rs.getString("nameFull"),
                    rs.getString("password"),
                    rs.getLong("age"),
                    rs.getString("city"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }



    public Optional<List<User>> findAll() {
        List<User> users = new ArrayList<>();
        String sqlAll = "SELECT * FROM usertrans1";
        try (PreparedStatement ps = connection.prepareStatement(sqlAll)) {
            ResultSet rs = ps.executeQuery();

            while(rs.next()){


                User user2 = (createUser(rs));

                users.add(user2);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.ofNullable(users);
    }

    @Override
    public void update() {

    }


    @Override
//    public void save(User model) {
//        try (PreparedStatement statement = connection.prepareStatement(
//                "INSERT INTO usertrans (iduser, nickname,email,avatarPath,namefull,password, age, city) VALUES (?,?,?,?,?,?,?,?)",
//                Statement.RETURN_GENERATED_KEYS)) {
//
//            statement.setInt(1,(new Random()).nextInt() * (int)1e5);
//            statement.setString(2, model.getNickname());
//            statement.setString(3, model.getEmail());
//
//            statement.setInt(4,(new Random()).nextInt() * (int)1e5);
//            statement.setString(5, model.getName());
//            statement.setString(6, model.getPassword());
//            statement.setLong(7, model.getAge());
//            statement.setString(8, model.getCity());
//            DaoHelper<User> daoHelper = new DaoHelper<>();
//            //daoHelper.setId(statement,model);
//            //пока будет рандом
//
//            daoHelper.checkingСhanges(statement);
//            statement.executeUpdate();
//
//
//
//        } catch (SQLException e) {
//            //Если сохранений провалилось, обернём пойманное исключение в непроверяемое и пробросим дальше(best-practise)
//            e.printStackTrace();
//        }
//    }
    public void save(User user) {


        if (user == null) throw new NullPointerException();
        String SQL_INSERT = "INSERT INTO " +
                "usertrans1 (nickname, email," +
                " avatarPath,namefull,password, age, city) VALUES (?, ?, ?, ?, ?, ?, ?)";
        try (PreparedStatement ps = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, user.getNickname());
            ps.setString(2, user.getEmail());
            ps.setString(3,user.getAvatar());
            ps.setString(4, user.getName());
            ps.setString(5, user.getPassword());
            ps.setLong(6, user.getAge());
            ps.setString(7, user.getCity());
            int affectedRows = ps.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException();
            }
            try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    user.setId(generatedKeys.getInt("iduser"));
                } else {
                    throw new SQLException();
                }
            }
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }
    @Override
    public void delete(long id) {

    }

    public User validateUser(String email, String pass) throws SQLException, ClassNotFoundException {

        PreparedStatement statement = connection.prepareStatement("SELECT * FROM usertrans1 " +
                "WHERE email = "+ "?"+" AND password = "+ "?");
        statement.setString(1, email);
        statement.setString(2, pass);
        ResultSet rs = statement.executeQuery();
        if (rs.next()) {
            User user = new User(
                    rs.getInt("idUser"),
                    rs.getString("nickname"),
                    rs.getString("email"),

                    rs.getString("avatarPath"),
                    rs.getString("nameFull"),
                    rs.getString("password"),
                    rs.getLong("age"),
                    rs.getString("city"));
            return user;
        }
        else return null;
    }




    public List<User> getByLikePattern(String pattern) {
        try {

            PreparedStatement ps = connection.prepareStatement(
                    "select * from usertrans1 where nickname like ?"
            );
            ps.setString(1, "%" + pattern + "%");
            ResultSet rs = ps.executeQuery();
            List<User> students = new ArrayList<>();
            while (rs.next()) {
                students.add(new User(
                        rs.getInt("idUser"),
                        rs.getString("nickname"),
                        rs.getString("email"),

                        rs.getString("avatarPath"),
                        rs.getString("nameFull"),
                        rs.getString("password"),
                        rs.getLong("age"),
                        rs.getString("city")));

            }
            return students;
        } catch (SQLException e) {
            e.printStackTrace();

        }
        return null;
    }


}
