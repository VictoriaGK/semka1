package dao;

import models.Message;
import models.Rent;
import models.Transport;
import models.User;
import services.TransService;
import services.UserService;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class RentDao {
    Connection connection;

    public RentDao(Connection connection) {
        this.connection = connection;
    }

    public void save(Rent rent) {


        if (rent == null) throw new NullPointerException();
        String SQL_INSERT = "INSERT INTO " +
                "renttrans (iduser, idtrans," +
                " telnumb) VALUES (?, ?, ?)";
        try (PreparedStatement ps = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            ps.setLong(1, rent.getIduser());
            ps.setLong(2, rent.getIdtrans());
            ps.setLong(3,rent.getTelNumb());
            ps.execute();

        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }


    private Rent createRent2(ResultSet rs) throws SQLException {
        Rent rent = null;
        TransService transService = new TransService();
        UserService userService = new UserService();
        Transport trans = transService.find(rs.getLong("idtrans"));
        User user  = userService.find(rs.getLong("iduser"));
        try {
            rent = new Rent(
                    rs.getLong("idrent"),
                    rs.getLong("telnumb"),
                    rs.getLong("idtrans"),
                    rs.getLong("iduser"));
            rent.setImgtrans(trans.getImagePath());
            rent.setNametrans(trans.getName());
            rent.setInfotrans(trans.getInfo());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rent;
    }


    public Rent find(Long val){
        Rent rent = null;
        try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM renttrans WHERE idrent"+" = ?")) {
            ps.setLong(1, val);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                rent = createRent(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rent;
    }

    public Optional<List<Rent>> findAllBy(String by, Long val) {
        List<Rent> rents = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM renttrans WHERE "+ by +" = ?")) {
            ps.setLong(1, val);
            ResultSet rs = ps.executeQuery();

            while(rs.next()){


                Rent rent = (createRent(rs));

                rents.add(rent);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.ofNullable(rents);
    }

    public Optional<List<Rent>> findAllBy2(String by, Long val) {
        List<Rent> rents = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM renttrans WHERE "+ by +" = ?")) {
            ps.setLong(1, val);
            ResultSet rs = ps.executeQuery();

            while(rs.next()){


                Rent rent = (createRent2(rs));

                rents.add(rent);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.ofNullable(rents);
    }

    public Optional<List<Rent>> findAllByUsers(User user) {

      long iduser = user.getId();
        List <Rent> rents = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM renttrans WHERE iduser" +" = ?")) {

            ps.setLong(1,iduser);

            ResultSet rs = ps.executeQuery();

            while(rs.next()){


                Rent rent = (createRent2(rs));

                rents.add(rent);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.ofNullable(rents);
    }

    private Rent createRent(ResultSet rs) throws SQLException {
        Rent rent = null;
        try {
            rent = new Rent(
                    rs.getLong("idrent"),
                    rs.getLong("telnumb"),
                    rs.getLong("idtrans"),
                    rs.getLong("iduser"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rent;
    }
}
