package dao;

import models.Magazin;
import models.Transport;
import models.User;
import servlets.TransServ;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MagazinDao {
    Connection connection;

    public MagazinDao(Connection connection) {
        this.connection = connection;
    }

    public Optional<List<Magazin>> findAllMagByTrans(Transport transport) {
        List<Magazin> magazins = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM transport WHERE idtrans "+"= ?",
                Statement.RETURN_GENERATED_KEYS)) {
            ps.setLong(1, transport.getId());
            System.out.println(ps);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                Magazin magazin = find(findID(rs));
                magazins.add(magazin);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.ofNullable(magazins);
    }

private int findID(ResultSet rs) throws SQLException{
    Magazin magazin = null;
    int idmag = rs.getInt("idmag");

    return idmag;

}

    private Magazin createMag(ResultSet rs) throws SQLException {
        Magazin magazin = null;
        try {

            magazin =  new Magazin(rs.getInt("idmag"),
                    rs.getString("namemag"),
                    rs.getString("imgmag"),
                    rs.getString("loc"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return magazin;
    }


    public Magazin find(long idmag) {
        System.out.println(idmag);
        Magazin magazin = null;
        try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM magazin WHERE idmag" +" = ?")) {

            ps.setLong(1,idmag);
            System.out.println(ps);
            ResultSet rs = ps.executeQuery();
            System.out.println(rs);
            if (rs.next()) {
                magazin  = createMag(rs);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return magazin;
    }


}
